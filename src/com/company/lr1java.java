package com.company;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        double a, j, m;
        int d = 32;
        double t = 273.15;
        double b = 1.8;
        Scanner u = new Scanner(System.in);
        System.out.println("Введите букву для преобразования(Ц-цельсий,Ф-Фарингейт,К-Кельвин)");
        String phrase1 = u.nextLine();
        switch (phrase1) {
            case "Ц":
                System.out.println("Введите градусы по цельсии:");
                a = new Scanner(System.in).nextDouble();
                j = a * b + d;
                m = a + t;
                System.out.println("Ваши градусы в Фаренгейтах:" + j);
                System.out.println("Ваши градусы в Кельвине:" + m);
                break;
            case "Ф":
                System.out.println("Введите градусы по фарингейту:");
                a = new Scanner(System.in).nextDouble();
                j = (a - d) / b;
                m = (a - d) * 5 / 9 + t;
                System.out.println("Ваши градусы в цельсиях:" + j);
                System.out.println("Ваши градусы в Кельвинах:" + m);
                break;
            case "К":
                System.out.println("Введите градусы по Кельвину:");
                a = new Scanner(System.in).nextDouble();
                j = a - t;
                m = (a - t) * 9 / 5 + d;
                System.out.println("Ваши градусы в цельсиях:" + j);
                System.out.println("Ваши градусы по Фарингейту:" + m);
                break;
            default:
                System.out.println("Вы неправильно ввели букву:");
                break;
        }
    }

}
